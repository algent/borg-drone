#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import argparse
import configparser
import json
import logging
import os
import re
import shlex
import smtplib
import sys
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from subprocess import Popen, PIPE
from time import strftime, time, gmtime


def main():
    # ### START MAIN ### #
    cwd = Path(__file__).parent

    # Parse command-line arguments
    parser = argparse.ArgumentParser(description='Borg Drone is a wrapper to help automate Borg Backup')
    parser.add_argument('-c', '--config-path', help='Path to config file if non default')
    parser.add_argument('-v', '--verbose', action='store_true', help='Debug log level on console')
    args = parser.parse_args()

    # Read configuration file
    if args.config_path is not None:
        config_file = Path(args.config_path)
    else:
        config_file = Path.joinpath(cwd, 'BorgDrone.ini')
    if not config_file.is_file():
        sys.exit(f'FATAL ERROR: Could not find config file at "{config_file}"')
    config = configparser.ConfigParser(interpolation=None)
    config.read(config_file)

    d_config = {
        'cwd': cwd,
        'bin_path': config.get('Borg', 'borg_bin_path'),
        'repo': config.get('Borg', 'borg_repository'),
        'prefix': config.get('Borg', 'borg_create_prefix'),
        'create_args': config.get('Borg', 'borg_create_args'),
        'create_path': config.get('Borg', 'borg_paths_to_archive'),
        'check_args': config.get('Borg', 'borg_check_args'),
        'prune_args': config.get('Borg', 'borg_prune_args'),
    }

    # Make sure borg_bin_path exist TODO Test version & allow empty line & try some fallback
    if not Path(d_config['bin_path']).is_file():
        sys.exit(f'FATAL ERROR: Could not find borg at "{d_config["bin_path"]}"')

    # Start Logger
    if config.has_option('Logs', 'log_dir') and config.get('Logs', 'log_dir'):
        log_dir = Path(config.get('Logs', 'log_dir'))
    else:
        log_dir = cwd.joinpath('log')
    if config.has_option('Logs', 'log_nb_to_keep'):
        log_nb_to_keep = config.get('Logs', 'log_nb_to_keep')
    else:
        log_nb_to_keep = 15
    if args.verbose:
        console_log_level = logging.DEBUG
    else:
        console_log_level = logging.INFO
    d_config['log_file'] = init_logger(log_dir, log_nb_to_keep, console_log_level)

    # Drone: Backup Mode
    run_backup(config, d_config)

    # ### END MAIN ### #
    exit(0)


# ## MAIN MODES ## #
def run_backup(config, d_config):
    # ## BEGIN BACKUP ##
    d_stats = {
        'job_name': config.get('General', 'job_name'),
        'subjobs_done': [],
        'time_started': strftime('%Y-%m-%d %H:%M:%S'),
        'time_started_nice': strftime('%A %d %B %Y %H:%M:%S'),
        'time_started_utc': datetime.utcnow(),
        'time_started_stopwatch': time(),
    }

    # Pre-job Script
    if config.has_option('General', 'prejob_script'):
        d_config['prejob_path'] = config.get('General', 'prejob_script')
        run_prejob_script(d_config, d_stats)

    logger.info(f'Starting backup job: {d_stats["job_name"]}')

    # TODO Move that to every exec for safety: https://stackoverflow.com/a/4453495
    os.environ['BORG_PASSPHRASE'] = config.get('Borg', 'borg_passphrase')
    # TODO support more env var https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables

    # Create new archive
    borg_create(d_config, d_stats)

    # Get json info of new archive
    borg_info_archive(d_config, d_stats)

    # Check backup integrity
    borg_check(d_config, d_stats)

    # Prune old archive according to retention arguments
    borg_prune(d_config, d_stats)

    # List all archives (text + json)
    borg_list(d_config, d_stats)

    # Get json infos on repository (after prune)
    borg_info_repo(d_config, d_stats)

    os.environ['BORG_PASSPHRASE'] = ""

    if config.has_option('General', 'postjob_script'):
        d_config['postjob_path'] = config.get('General', 'postjob_script')
        run_postjob_script(d_config, d_stats)

    d_stats['time_ended'] = strftime('%Y-%m-%d %H:%M:%S')
    d_stats['time_ended_nice'] = strftime('%A %d %B %Y %H:%M:%S')
    d_stats['time_elapsed'] = round(time() - d_stats['time_started_stopwatch'], 2)
    d_stats['time_elapsed_nice'] = '{:02d}:{:02d}:{:02d}'.format(int(d_stats['time_elapsed']) // 3600,
                                                                 int(d_stats['time_elapsed']) % 3600 // 60,
                                                                 int(d_stats['time_elapsed']) % 60)

    get_job_rc(d_stats)

    logger.debug('List of sub-jobs executed: {}'.format(d_stats['subjobs_done']))
    logger.info(f'End of backup job in {d_stats["time_elapsed_nice"]} with status: {d_stats["job_rc_txt"]}')

    # ## END BACKUP   ##

    # ## HIVE REPORT ##
    # TODO Send job data and repo stats to Borg Hive
    # ## HIVE REPORT   ##

    # ## BEGIN REPORT ##
    if config.getboolean('Reports', 'report_enable'):
        d_config['report_enabled'] = True
        report_template_file = d_config['cwd'].joinpath('BorgDrone-report.html')
        if not report_template_file.is_file():
            logger.critical('Could not find report template file "BorgDrone-report.html"')
        with open(report_template_file, 'r') as report_body_template:
            report_body_html = report_body_template.read()
        d_config['report_from'] = config.get('Reports', 'report_from')
        d_config['report_to'] = config.get('Reports', 'report_to')
        d_config['report_subject'] = config.get('Reports', 'report_subject')
        d_config['smtp_host'] = config.get('Reports', 'smtp_host')
        d_config['smtp_port'] = config.get('Reports', 'smtp_port') if config.has_option('Reports',
                                                                                        'smtp_port') else 25
        d_config['smtp_user'] = config.get('Reports', 'smtp_user') if config.has_option('Reports',
                                                                                        'smtp_user') else ''
        d_config['smtp_pwd'] = config.get('Reports', 'smtp_pwd') if config.has_option('Reports', 'smtp_pwd') else ''
        build_email_report(d_config, d_stats, report_body_html)

    # ## END REPORT   ##
    return


# ## FUNCTIONS ## #
# pre-job Script
def run_prejob_script(d_config, d_stats):
    logger.info('DRONE PRE-JOB - Running pre-job script')
    d_stats['prejob_rc'], d_stats['prejob_stdout'], d_stats['prejob_stderr'] = run_cmd_get_output(
        d_config['prejob_path'])
    d_stats['prejob_rc_txt'] = rc_to_string(d_stats["prejob_rc"])
    d_stats['subjobs_done'].append('prejob')
    # TODO Detect empty output and set as None/Null ?
    logger.debug(f'DRONE PRE-JOB - STDOUT:\n{d_stats["prejob_stdout"]}')
    logger.debug(f'DRONE PRE-JOB - STDERR:\n{d_stats["prejob_stderr"]}')
    logger.info(f'DRONE PRE-JOB - STATUS: {d_stats["prejob_rc_txt"]} - [RC {d_stats["prejob_rc"]}]')
    logger.info('DRONE PRE-JOB - END')


# Borg create - create new archive
def borg_create(d_config, d_stats):
    logger.info('BORG CREATE - Creating Archive')
    d_stats['archive'] = f'{d_config["repo"]}::{d_config["prefix"]}-{strftime("%Y-%m-%d_%H-%M")}'
    create_cmd = f'{d_config["bin_path"]} create {d_config["create_args"]} ' \
        f'{d_stats["archive"]} {d_config["create_path"]}'
    d_stats['create_rc'], d_stats['create_stdout'], d_stats['create_stderr'] = run_cmd_get_output(create_cmd)
    d_stats['create_rc_txt'] = rc_to_string(d_stats["create_rc"])
    d_stats['subjobs_done'].append('create')
    logger.info(f'BORG CREATE - OUTPUT:\n{d_stats["create_stderr"]}')
    logger.info(f'BORG CREATE - STATUS: {d_stats["create_rc_txt"]} - [RC {d_stats["create_rc"]}]')
    logger.info('BORG CREATE - END')


# Borg info - get json info of new archive
def borg_info_archive(d_config, d_stats):
    logger.info('BORG INFO ARCHIVE - Getting information on new archive')
    info_archive_cmd = f'{d_config["bin_path"]} info --json {d_stats["archive"]}'
    d_stats['info_archive_rc'], d_stats['info_archive_stdout'], d_stats['info_archive_stderr'] = run_cmd_get_output(
        info_archive_cmd)
    # TODO Error Handling on json loads (Make it empty ? Make sure we don't use it ?)
    d_stats['info_archive_json'] = json.loads(d_stats['info_archive_stdout'])
    d_stats['info_archive_rc_txt'] = rc_to_string(d_stats["info_archive_rc"])
    d_stats['subjobs_done'].append('info_archive')
    logger.debug('BORG INFO ARCHIVE - OUTPUT:\n{}'.format(json.dumps(d_stats['info_archive_json'])))
    logger.info(f'BORG INFO ARCHIVE - STATUS: {d_stats["info_archive_rc_txt"]} - [RC {d_stats["info_archive_rc"]}]')
    logger.info('BORG INFO ARCHIVE - END')


# Borg check - Look for anomalies in archive
def borg_check(d_config, d_stats):
    logger.info('BORG CHECK - Verifying archives consistency')
    check_cmd = f'{d_config["bin_path"]} check {d_config["check_args"]} {d_config["repo"]}'
    d_stats['check_rc'], d_stats['check_stdout'], d_stats['check_stderr'] = run_cmd_get_output(check_cmd)
    d_stats['check_stderr'] = os.linesep.join([s for s in d_stats['check_stderr'].splitlines() if s])
    d_stats['check_rc_txt'] = rc_to_string(d_stats["check_rc"])
    d_stats['subjobs_done'].append('check')
    logger.info(f'BORG CHECK - OUTPUT:\n{d_stats["check_stderr"]}')
    logger.info(f'BORG CHECK - STATUS: {d_stats["check_rc_txt"]} - [RC {d_stats["check_rc"]}]')
    logger.info('BORG CHECK - END')


# Borg prune - Delete old archives with same prefix as create job
def borg_prune(d_config, d_stats):
    logger.info('BORG PRUNE - Deleting old archives')
    prune_cmd = f'{d_config["bin_path"]} prune {d_config["prune_args"]} -P {d_config["prefix"]} {d_config["repo"]}'
    d_stats['prune_rc'], d_stats['prune_stdout'], d_stats['prune_stderr'] = run_cmd_get_output(prune_cmd)
    d_stats['prune_rc_txt'] = rc_to_string(d_stats["prune_rc"])
    d_stats['subjobs_done'].append('prune')
    logger.info(f'BORG PRUNE - OUTPUT:\n{d_stats["prune_stderr"]}')
    logger.info(f'BORG PRUNE - STATUS: {d_stats["prune_rc_txt"]} - [RC {d_stats["prune_rc"]}]')
    logger.info('BORG PRUNE - END')


# Borg List - Listing archives in repository
def borg_list(d_config, d_stats):
    logger.info('BORG LIST - Listing all archives')

    list_json_cmd = f'{d_config["bin_path"]} list --json {d_config["repo"]}'
    d_stats['list_json_rc'], d_stats['list_json_stdout'], d_stats['list_json_stderr'] = run_cmd_get_output(
        list_json_cmd)
    d_stats['list_json'] = json.loads(d_stats['list_json_stdout'])
    d_stats['list_json_rc_txt'] = rc_to_string(d_stats["list_json_rc"])
    d_stats['subjobs_done'].append('list_json')
    logger.debug('BORG LIST JSON - OUTPUT:\n{}'.format(json.dumps(d_stats['list_json'])))
    logger.info(f'BORG LIST JSON - STATUS: {d_stats["list_json_rc_txt"]} - [RC {d_stats["list_json_rc"]}]')

    list_txt_cmd = f'{d_config["bin_path"]} list -v {d_config["repo"]}'
    d_stats['list_txt_rc'], d_stats['list_txt_stdout'], d_stats['list_txt_stderr'] = run_cmd_get_output(list_txt_cmd)
    d_stats['list_txt_stdout'] = os.linesep.join([s for s in d_stats['list_txt_stdout'].splitlines() if s])
    d_stats['list_txt_rc_txt'] = rc_to_string(d_stats["list_txt_rc"])
    d_stats['subjobs_done'].append('list_txt')
    logger.info(f'BORG LIST TXT - OUTPUT:\n{d_stats["list_txt_stdout"]}')
    logger.info(f'BORG LIST TXT - STATUS: {d_stats["list_txt_rc_txt"]} - [RC {d_stats["list_txt_rc"]}]')

    logger.info('BORG LIST - END')


# Borg info - Getting json infos of repository
def borg_info_repo(d_config, d_stats):
    logger.info('BORG INFO REPO - Getting informations on repository')
    info_repo_cmd = f'{d_config["bin_path"]} info --json {d_config["repo"]}'
    d_stats['info_repo_rc'], d_stats['info_repo_stdout'], d_stats['info_repo_stderr'] = run_cmd_get_output(
        info_repo_cmd)
    d_stats['info_repo_json'] = json.loads(d_stats['info_repo_stdout'])
    d_stats['info_repo_rc_txt'] = rc_to_string(d_stats["info_repo_rc"])
    d_stats['subjobs_done'].append('info_repo')
    logger.debug('BORG INFO REPO - OUTPUT:\n{}'.format(json.dumps(d_stats['info_repo_json'])))
    logger.info(f'BORG PRUNE - STATUS: {d_stats["info_repo_rc_txt"]} - [RC {d_stats["info_repo_rc"]}]')
    logger.info('BORG INFO REPO - END')


# post-job Script
def run_postjob_script(d_config, d_stats):
    logger.info('DRONE POST-JOB - Running post-job script')
    d_stats['postjob_rc'], d_stats['postjob_stdout'], d_stats['postjob_stderr'] = run_cmd_get_output(
        d_config['postjob_path'])
    d_stats['postjob_rc_txt'] = rc_to_string(d_stats["postjob_rc"])
    d_stats['subjobs_done'].append('postjob')
    logger.debug(f'DRONE POST-JOB - STDOUT:\n{d_stats["postjob_stdout"]}')
    logger.debug(f'DRONE POST-JOB - STDERR:\n{d_stats["postjob_stderr"]}')
    logger.info(f'DRONE POST-JOB - STATUS: {d_stats["postjob_rc_txt"]} - [RC {d_stats["postjob_rc"]}]')
    logger.info('DRONE POST-JOB - END')


def build_email_report(d_config, d_stats, report_body_html):
    logger.info('Building email report...')

    # Name of Sub Jobs
    sub_job_names = {
        'prejob': 'Pre-job Script',
        'create': 'Borg Create',
        'info_archive': 'Borg Info (Archive)',
        'check': 'Borg Check',
        'prune': 'Borg Prune',
        'list_json': 'Borg List (json)',
        'list_txt': 'Borg List (txt)',
        'info_repo': 'Borg Info (Repository)',
        'postjob': 'Post-job Script',
    }

    # Round up stats we need for report
    d_report = {
        'hostname': d_stats['info_archive_json']['archives'][0]['hostname'],
        'nfiles': d_stats['info_archive_json']['archives'][0]['stats']['nfiles'],
        'original_size': d_stats['info_archive_json']['archives'][0]['stats']['original_size'],
        'compressed_size': d_stats['info_archive_json']['archives'][0]['stats']['compressed_size'],
        'deduplicated_size': d_stats['info_archive_json']['archives'][0]['stats']['deduplicated_size'],
        'total_size': d_stats['info_repo_json']['cache']['stats']['total_size'],
        'total_csize': d_stats['info_repo_json']['cache']['stats']['total_csize'],
        'unique_csize': d_stats['info_repo_json']['cache']['stats']['unique_csize'],
    }
    d_report['c_ratio'] = safe_divide(d_report['original_size'], d_report['compressed_size'])
    d_report['c_ratio_saving'] = (1 - (safe_divide(d_report['compressed_size'], d_report['original_size']))) * 100
    d_report['d_ratio'] = safe_divide(d_report['original_size'], d_report['deduplicated_size'])
    d_report['d_ratio_saving'] = (1 - (safe_divide(d_report['deduplicated_size'], d_report['original_size']))) * 100
    d_report['tc_ratio'] = d_report['total_size'] / d_report['total_csize']
    d_report['tc_ratio_saving'] = (1 - (d_report['total_csize'] / d_report['total_size'])) * 100
    d_report['td_ratio'] = d_report['total_size'] / d_report['unique_csize']
    d_report['td_ratio_saving'] = (1 - (d_report['unique_csize'] / d_report['total_size'])) * 100

    # Grab content of log file
    with open(d_config['log_file'], 'r') as log_file:
        log_file_content = log_file.read()

    # Grab color codes from file
    grc_colors_json = json.loads(re.search(r'%%GRC_COLOR_START%%([\s\S]*)%%GRC_COLOR_END%%', report_body_html).group(1))
    job_colors_json = json.loads(re.search(r'%%JOB_COLOR_START%%([\s\S]*)%%JOB_COLOR_END%%', report_body_html).group(1))

    # Grab sub-job bloc from template
    sub_job_bloc = ''
    sub_job_bloc_re = r'<!--%%SUBJOB_BLOC_START%%-->([\s\S]*)<!--%%SUBJOB_BLOC_END%%-->'
    job_bloc_tpl = re.search(sub_job_bloc_re, report_body_html).group(1)

    for subjob in d_stats['subjobs_done']:
        sub_job_bloc += job_bloc_tpl.replace('%%SUBJOB_NAME%%', sub_job_names[subjob], 1) \
                            .replace('%%SUBJOB_COLOR%%', job_colors_json[d_stats[subjob + '_rc_txt']], 1) \
                            .replace('%%SUBJOB_RESULT%%', d_stats[subjob + '_rc_txt'], 1) \
                            .replace('%%SUBJOB_DETAILS%%', '', 1) + '\n'
        continue

    # Replace all placeholders in report body with values
    report_body_html = re.sub(sub_job_bloc_re, sub_job_bloc, report_body_html)
    report_body_html = report_body_html.replace('%%JOBNAME%%', d_stats['job_name'], 2) \
        .replace('%%GLOBALRESULT%%', d_stats['job_rc_txt'], 1) \
        .replace('%%GLOBALRESULT_COLOR%%', grc_colors_json[d_stats['job_rc_txt']], 1) \
        .replace('%%NICETIME%%', d_stats['time_started_nice'], 1) \
        .replace('%%HOSTNAME%%', d_report['hostname'], 1) \
        .replace('%%STARTTIME%%', d_stats['time_started'], 1) \
        .replace('%%ENDTIME%%', d_stats['time_ended'], 1) \
        .replace('%%DURATION%%', d_stats['time_elapsed_nice'], 1) \
        .replace('%%NFILES%%', '{:,}'.format(d_report['nfiles']).replace(',', ' '), 1) \
        .replace('%%OSIZE%%', sizeof_fmt_decimal(d_report['original_size']), 1) \
        .replace('%%CSIZE%%', sizeof_fmt_decimal(d_report['compressed_size']), 1) \
        .replace('%%DSIZE%%', sizeof_fmt_decimal(d_report['deduplicated_size']), 1) \
        .replace('%%CRATIO%%', f'{d_report["c_ratio"]:.2f}:1&nbsp;&nbsp;&nbsp;[{d_report["c_ratio_saving"]:.2f}%]', 1) \
        .replace('%%DRATIO%%', f'{d_report["d_ratio"]:.1f}:1&nbsp;&nbsp;&nbsp;[{d_report["d_ratio_saving"]:.2f}%]', 1) \
        .replace('%%TOSIZE%%', sizeof_fmt_decimal(d_report['total_size']), 1) \
        .replace('%%TCSIZE%%', sizeof_fmt_decimal(d_report['total_csize']), 1) \
        .replace('%%TDSIZE%%', sizeof_fmt_decimal(d_report['unique_csize']), 1) \
        .replace('%%TCRATIO%%',
                 f'{d_report["tc_ratio"]:.2f}:1&nbsp;&nbsp;&nbsp;[{d_report["tc_ratio_saving"]:.2f}%]', 1) \
        .replace('%%TDRATIO%%',
                 f'{d_report["td_ratio"]:.1f}:1&nbsp;&nbsp;&nbsp;[{d_report["td_ratio_saving"]:.2f}%]', 1) \
        .replace('%%FULL_LOG%%', log_file_content.replace('\n', '\n<br/>').replace(' ', '&nbsp;'), 1)
    # TODO improve nbsp usage in TCRATIO & TDRATIO

    # Little text report in case html isn't possible
    report_body_text = f'[Backup Report] Job: {d_stats["job_name"]} [{d_stats["job_rc_txt"]}]\n' \
        f'Start Time: {d_stats["time_started_nice"]}\n' \
        f'Duration: {strftime("%H:%M:%S", gmtime(d_stats["time_elapsed"]))}\n'

    # Replace placeholders in mail headers
    d_config['report_from'] = d_config['report_from'].replace('%%JOBNAME%%', d_stats['job_name'], 1)
    d_config['report_subject'] = d_config['report_subject'].replace('%%JOBNAME%%', d_stats['job_name'], 1) \
        .replace('%%GLOBALRESULT%%', d_stats['job_rc_txt'], 1)

    # Send report we just built
    send_email(d_config['report_from'], d_config['report_to'], d_config['report_subject'],
               report_body_html, report_body_text, d_config['smtp_host'], smtp_port=d_config['smtp_port'],
               smtp_user=d_config['smtp_user'], smtp_pwd=d_config['smtp_pwd'])


def send_email(send_from, send_to, msg_subject, msg_body_html, msg_body_text, smtp_host, smtp_port=25, smtp_user='',
               smtp_pwd=''):
    msg = MIMEMultipart('alternative')
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Subject'] = msg_subject
    msg['Date'] = formatdate(localtime=True)
    msg['Message-Id'] = make_msgid()
    msg_text = MIMEText(msg_body_text, 'plain', 'utf-8')
    msg.attach(msg_text)
    msg_html = MIMEText(msg_body_html, 'html', 'utf-8')
    msg.attach(msg_html)

    try:
        if smtp_port == 465:
            logger.debug('[SMTP] SSL port detected: Using SSL mode.')
            smtp_session = smtplib.SMTP_SSL(smtp_host)
        else:
            smtp_session = smtplib.SMTP(smtp_host, smtp_port)

        if smtp_port == 587:
            logger.debug('[SMTP] TLS port detected: Switching to TLS mode.')
            smtp_session.starttls()

        if not smtp_user == '':
            logger.debug('[SMTP] Authenticating with server...')
            smtp_session.login(smtp_user, smtp_pwd)

        smtp_session.sendmail(send_from, shlex.split(send_to.replace(', ', ' ')), msg.as_string())
        smtp_session.quit()
        logger.info('[SMTP] Successfully sent email.')
    except smtplib.socket.error:
        logger.error('[SMTP] Connection timed out. Please verify smtp_host value and your firewall settings.')
    except smtplib.SMTPException as e:
        logger.error('[SMTP] An exception of type "{}" occurred. Output:\n{}'.format(e.__class__.__name__, e.args))


# ## Utils ## #

# Run bash cmd and return outputs
def run_cmd_get_output(cmd, cwd=None, shell=False, log=True):
    if log:
        logger.debug('EXEC - {}'.format(cmd))
    if shell:
        args = cmd
    else:
        args = shlex.split(cmd)
    proc = Popen(args, stdout=PIPE, stderr=PIPE, cwd=cwd, shell=shell)
    stdout, stderr = proc.communicate()
    exitcode = proc.returncode
    return exitcode, stdout.decode(), stderr.decode()


# RC to string
def rc_to_string(rc: int):
    if rc == 0:
        return 'Success'
    elif rc == 1:
        return 'Warning'
    elif rc == 2:
        return 'Error'
    else:
        return 'Unknown'


# Calculate RC for job
def get_job_rc(d_stats):
    d_stats['job_rc'] = 0
    d_stats['job_rc_txt'] = 'Success'
    for subjob in d_stats['subjobs_done']:
        if d_stats[subjob + '_rc'] == 0:
            continue
        elif d_stats[subjob + '_rc'] == 1:
            d_stats['job_rc'] = 1
            d_stats['job_rc_txt'] = 'Warning'
        else:
            d_stats['job_rc'] = 2
            d_stats['job_rc_txt'] = 'Error'
            return


def sizeof_fmt(num, suffix='B', units=None, power=None, sep='', precision=2, sign=False):
    if num is None:
        return 'Null'
    prefix = '+' if sign and num > 0 else ''

    for unit in units[:-1]:
        if abs(round(num, precision)) < power:
            if isinstance(num, int):
                return "{}{}{}{}{}".format(prefix, num, sep, unit, suffix)
            else:
                return "{}{:3.{}f}{}{}{}".format(prefix, num, precision, sep, unit, suffix)
        num /= float(power)
    return "{}{:.{}f}{}{}{}".format(prefix, num, precision, sep, units[-1], suffix)


def sizeof_fmt_decimal(num, suffix='B', sep=' ', precision=2, sign=False):
    return sizeof_fmt(num, suffix=suffix, sep=sep, precision=precision, sign=sign,
                      units=['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'], power=1000)


def safe_divide(x: int, y: int):
    if y:
        return x / y
    else:
        return 0


# ## INIT ## #

# Init logger
def init_logger(log_dir=Path.cwd(), nb_to_keep=15, console_log_level=logging.INFO):
    log_dir.mkdir(parents=True, exist_ok=True)
    log_file = log_dir.joinpath(f'borg_drone_{strftime("%Y-%m-%dT%H-%M-%S")}.log')
    logger.setLevel(logging.DEBUG)
    fh = TimedRotatingFileHandler(log_file, when='M', interval=14400, backupCount=nb_to_keep)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter('%(asctime)s :: %(levelname)-8s :: %(message)s', datefmt='%y-%m-%d %H:%M'))
    logger.addHandler(fh)
    console = logging.StreamHandler()
    console.setLevel(console_log_level)
    console.setFormatter(logging.Formatter('%(asctime)s :: %(levelname)-8s :: %(message)s', datefmt='%y-%m-%d %H:%M'))
    logger.addHandler(console)
    return log_file


if __name__ == '__main__':
    if os.name == 'nt':
        sys.exit("FATAL ERROR: Borg Drone isn't made to run on Windows")
    logger = logging.getLogger('borg_drone')
    main()
