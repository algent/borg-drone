# Borg Drone

### What is Borg Drone?

Borg Drone is a script made to help automate the use of the 
[BorgBackup](https://www.borgbackup.org/) deduplicating backup program.


### Requirement
Python 3.6 or higher


### Installation
- Copy the following files in a folder: BorgDrone.py, BorgDrone.ini, BorgDrone-report.html.
- Use the ini file to configure the job.


### Features

- Use BorgBackup to create archives, check repository integrity and prune old data.
- Send job report via email.
- Ability to run script pre and post job.


### Future Features Ideas

- Improve email template: Cleaner Sub-Jobs with some details/time elapsed.
- Some way to define which Sub-Jobs to run (config with default value ).
- Keep data on job not sent to retry next time Drone is launched.
- Send job report to Borg Hive.
- Collection mode to get stats on every archive inside repository.
- Test mode: Check configuration, test smtp, borg version, ... ?
- Register mode: Register drone to Hive and get some api key (pre drone ? global ?).
- Support more Borg options like alternative SSH port or env var.
- Check mode: Only run full check of whole repository.
- Init mode: Create repository.
- Calculate size of pruned data (prune doesn't output clean json).

