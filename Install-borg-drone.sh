#!/usr/bin/env bash

centos_install_drone(){
centos_install_python36
}

centos_install_python36(){
yum install -y epel-release
yum install -y python36
}

centos_install_python37(){
yum install -y gcc openssl-devel bzip2-devel  libffi-devel

PYTHON_VERSION="3.7.2"
cd /usr/src
wget "https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz"
tar xzf "Python-${PYTHON_VERSION}.tgz"
cd "Python-${PYTHON_VERSION}"

./configure --enable-optimizations
make altinstall

rm -f "/usr/src/Python-${PYTHON_VERSION}.tgz"

python3.7 -V
cd ~
}

install_borg(){
wget https://github.com/borgbackup/borg/releases/download/1.1.8/borg-linux64
/bin/cp borg-linux64 /usr/local/bin/borg
chown root:root /usr/local/bin/borg
chmod 755 /usr/local/bin/borg
rm -f borg-linux64
borg --version
}


## MAIN

if [[ -e /etc/redhat-release ]]; then
	RELEASE_RPM=$(rpm -qf /etc/redhat-release)
	RELEASE=$(rpm -q --qf '%{VERSION}' ${RELEASE_RPM})
	case ${RELEASE_RPM} in
		centos*) centos_install_drone ;;
		*) exit 1 ;;
	esac
else
	exit 1
fi
